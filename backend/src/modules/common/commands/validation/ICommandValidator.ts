import {IValidationResult} from "./IValidationResult";

export interface ICommandValidator<TCommand> {

    validate(command: TCommand): Promise<IValidationResult>
}
