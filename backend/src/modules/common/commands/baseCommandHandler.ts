import {ICommand} from "./ICommand";
import {ICommandHandler} from "./ICommandHandler";


export abstract class BaseCommandHandler<TCommand extends ICommand, TResult> implements ICommandHandler<TCommand, TResult> {

    execute(command: TCommand, resolve: (value?) => void): Promise<any> | any {

        return this.handle(command)
            .then(value => resolve(value));

    }

    abstract handle(command: TCommand): Promise<TResult>

}
