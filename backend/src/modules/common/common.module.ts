import {Module} from "@nestjs/common";
import {SyncCommandDispatcher} from "./commands";
import {SyncEventDispatcher} from "./events";
import { CQRSModule } from '@nestjs/cqrs';

@Module({
    imports: [CQRSModule],
    providers: [SyncCommandDispatcher, SyncEventDispatcher],
    exports: [ SyncCommandDispatcher, SyncEventDispatcher],
})
export class CommonModule {

}
