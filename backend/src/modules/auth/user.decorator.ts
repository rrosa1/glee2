import { createParamDecorator } from '@nestjs/common';
import { JwtPayload } from './jwt-payload.interface';

/**
 * retrieve the current user with a decorator
 * example of a controller method:
 * @Post()
 * someMethod(@Usr() user: User) {
 *   // do something with the user
 * }
 */
export const Usr = createParamDecorator((_, req) => {
  const user: JwtPayload = {
    username: req.user.sub,
  };
  return user;
});
