import { NestMiddleware, Injectable, Inject, MiddlewareFunction } from '@nestjs/common';
import * as jwt from 'express-jwt';
import { expressJwtSecret } from 'jwks-rsa';
import { ConfigService } from '../config/config.service';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
  constructor(
    @Inject('ConfigService') private readonly config: ConfigService,
  ) {}
  resolve() {
    const issuer = `https://${this.config.AUTH0_DOMAIN}/`;
    const jwksUri = `https://${this.config.AUTH0_DOMAIN}/.well-known/jwks.json`;
    return jwt({
      secret: expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri,
      }),
      aud: this.config.AUTH0_AUDIENCE,
      issuer,
      algorithm: 'RS256',
    });
  }
  // resolve(...args: any[]): MiddlewareFunction {
  //   const issuer = `https://${this.config.AUTH0_DOMAIN}/`;
  //   const jwksUri = `https://${this.config.AUTH0_DOMAIN}/.well-known/jwks.json`;
  //   return (req, res, next) => {
  //     console.log(req);
  //     next();
  //     // jwt({
  //     //   secret: expressJwtSecret({
  //     //     cache: true,
  //     //     rateLimit: true,
  //     //     jwksRequestsPerMinute: 5,
  //     //     jwksUri,
  //     //   }),
  //     //   aud: this.config.AUTH0_AUDIENCE,
  //     //   issuer,
  //     //   algorithm: 'RS256',
  //     // });
  //   }
  // }
}
