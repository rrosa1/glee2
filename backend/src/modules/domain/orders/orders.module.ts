import { Module, OnModuleInit } from '@nestjs/common';
import { CommandHandlers } from './commands/handlers';
import { CommonModule } from '../../common';
import { EventHandlers } from './events/handlers';

import { ModuleRef } from '@nestjs/core';
import { AppLogger } from '../../app/app.logger';

import { ProductRepository } from './repositories/productRepository';
import { OrderRepository } from './repositories/orderRepository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrdersController } from './orders.controller';
import { Order } from './entities/order.entity';
import { Product } from './entities/product.entity';
import {SyncCommandDispatcher} from "../../common/commands";
import {SyncEventDispatcher} from "../../common/events";
import {CommandValidators} from "./commands/validators";

@Module({
    imports: [
        CommonModule,
        TypeOrmModule.forFeature([Product, Order]),
    ],
    controllers: [OrdersController],
    providers: [ProductRepository, OrderRepository,...CommandHandlers, ...EventHandlers, ...CommandValidators ,AppLogger],
})
export class OrdersModule implements OnModuleInit {
    constructor(
        private readonly moduleRef: ModuleRef,
        private readonly command$: SyncCommandDispatcher,
        private readonly event$: SyncEventDispatcher,
    ) {}

    onModuleInit() {

        this.command$.setModuleRef(this.moduleRef);
        this.event$.setModuleRef(this.moduleRef);



        this.event$.register(EventHandlers);
        this.command$.register(CommandHandlers);
        this.command$.registerValidators(CommandValidators);
    }
}
