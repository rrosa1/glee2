import {SyncCommandDispatcher} from "../../common/commands";
import {OrderRequest} from "./requests/orderRequest";
import {Body, Controller, Get, Post, Query} from "@nestjs/common";
import {Usr} from "../../auth/user.decorator";
import {CreateOrder} from "./commands/createOrder";
import {OrderRepository} from "./repositories/orderRepository";
import {PaginatedOrderQuery} from "./requests/PaginatedOrderQuery";
import {ProductRepository} from "./repositories/productRepository";
import {PaginatedProductQuery} from "./requests/paginatedProductQuery";


@Controller('/orders')
export class OrdersController {

    constructor(
        private readonly commandDispatcher: SyncCommandDispatcher,
        private readonly orderRepository: OrderRepository) {

    }

    @Post()
    async createOrder(@Body()orderRequest: OrderRequest) {

        const createOrder: CreateOrder = new CreateOrder(orderRequest.productId, orderRequest.productQuantity, 'userId');
        await this.commandDispatcher.execute(createOrder);

        return null;
    }

    @Get()
    async getOrders(@Query() query: PaginatedOrderQuery) {

        return this.orderRepository
            .where({id: query.id})
            .relation(query.relations)
            .paginate(query.page, query.perPage);
    }


}
