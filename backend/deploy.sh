#!/usr/bin/env bash
set -x
set -e

echo "Deploying backend to ${EB_ENV_NAME} for review..."

jq < Dockerrun.aws.template.json ".Image.Name=\"${CONTAINER_IMAGE}:${CI_BUILD_REF}\""  > Dockerrun.aws.json

cat Dockerrun.aws.json

if [ ! -z "$(eb list | grep "${EB_ENV_NAME}")" ]
then
    echo "Updating existing environment"
    eb use "${EB_ENV_NAME}"
    eb setenv NPM_TOKEN=${NPM_TOKEN} \
        VERSION=${CI_COMMIT_REF_SLUG} \
        NODE_ENV=${NODE_ENV} \
        TYPEORM_CONNECTION=${TYPEORM_CONNECTION} \
        TYPEORM_HOST=${TYPEORM_HOST} \
        TYPEORM_PORT=${TYPEORM_PORT} \
        TYPEORM_USERNAME=${TYPEORM_USERNAME} \
        TYPEORM_PASSWORD=${TYPEORM_PASSWORD} \
        TYPEORM_DATABASE=${TYPEORM_DATABASE} \
        TZ=${TZ} \
        AUTH0_AUDIENCE=${AUTH0_AUDIENCE} \
        AUTH0_DOMAIN=${AUTH0_DOMAIN}         
    eb deploy "${EB_ENV_NAME}" | tee "eb_deploy_output.txt"

else
    echo "Creating new environment"
    eb create "${EB_ENV_NAME}" \
        --single \
        --envvars NPM_TOKEN=${NPM_TOKEN},VERSION=${CI_COMMIT_REF_SLUG},NODE_ENV=${NODE_ENV},TYPEORM_CONNECTION=${TYPEORM_CONNECTION},TYPEORM_HOST=${TYPEORM_HOST},TYPEORM_PORT=${TYPEORM_PORT},TYPEORM_USERNAME=${TYPEORM_USERNAME},TYPEORM_PASSWORD=${TYPEORM_PASSWORD},TYPEORM_DATABASE=${TYPEORM_DATABASE},TZ=${TZ},AUTH0_AUDIENCE=${AUTH0_AUDIENCE},AUTH0_DOMAIN=${AUTH0_DOMAIN}| tee "eb_deploy_output.txt"
fi

# Temporary hack to overcome issue with 'eb deploy' returning exit code 0 on error
# See http://stackoverflow.com/questions/23771923/elasticbeanstalk-deployment-error-command-hooks-directoryhooksexecutor-py-p

if grep -c -q -i error: "eb_deploy_output.txt"
then    
    cat eb_deploy_output.txt
    echo 'Error found in deploy log.'
    exit 1
fi
