import * as React from 'react';
// import style from './style.local.css';
import { Navbar } from './index';
// import { NavbarButton } from "../NavbarButton";
import { AuthService } from '../../services/auth';
import { createMemoryHistory } from 'history';
import { shallow } from 'enzyme';

jest.mock('./style.local.css', () => ({
  default: { disabled: 'disabled' },
}));

const mockAuthService: any = jest.fn<AuthService>(() => ({
  logout: jest.fn(),
  auth0: {},
}));

const memoryHistory = createMemoryHistory();

describe('<Navbar />', () => {
  let wrapper = shallow(
    <Navbar authService={mockAuthService} history={memoryHistory} />,
  );

  describe('Given that the employees view is active', () => {
    beforeAll(() => {
      memoryHistory.push('/employees');
      wrapper = wrapper.setState({ isOrdersActive: true });
    });

    // it('The Navbar button for Employees should not have the "disabled" class', () => {
    //   expect(
    //     wrapper
    //       .find(NavbarButton)
    //       .first()
    //       .hasClass("disabled")
    //   ).toBe(false);
    // });
    // it('The Navbar button for Inventory should have the "disabled" class', () => {
    //   expect(
    //     wrapper
    //       .find(NavbarButton)
    //       .at(1)
    //       .hasClass("disabled")
    //   ).toBe(true);
    // });

    describe('When I clicked on the Navbar button for Employees', () => {
      it('Should stay on the Employees view', () => {
        expect(
          (wrapper.instance() as Navbar).shouldViewChange('/employees'),
        ).toBe(false);
      });
    });
  });
});
