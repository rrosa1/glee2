import { RouterState } from 'react-router-redux';
import { EmployeeModel } from 'app/models/EmployeeModel';

export interface RootState {
  router: RouterState;
  employees: RootState.EmployeesState;
}

export interface SortedColumn {
  id: string;
  desc: boolean;
}

export namespace RootState {
  export interface EmployeesState {
    isFetching: boolean;
    errorMessage: string;
    employees: EmployeeModel[];
  }
}
