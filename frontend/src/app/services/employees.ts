import { HttpService } from './httpService';
import { EmployeeModel, EmployeeUpdateModel } from '../models/EmployeeModel';

export class EmployeesService {
  private readonly employeesUrl: string;
  private readonly httpService: HttpService;
  constructor(baseUrl = process.env.API_URL) {
    this.employeesUrl = `${baseUrl}/v1/employees`;
    this.httpService = new HttpService();
  }

  async getAllEmployees(filteredBy?: string): Promise<GetEmployeesResponse> {
    const response = await this.httpService.get(this.employeesUrl, {
      params: {
        filter: filteredBy,
      },
    });
    return {
      employees: response,
    };
  }

  createEmployee(employee: EmployeeModel) {
    return this.httpService.post(this.employeesUrl, employee);
  }

  async updateEmployee(
    id: number,
    employee: EmployeeUpdateModel,
  ): Promise<void> {
    return this.httpService.put(`${this.employeesUrl}/${id}`, employee);
  }

  async updateEmployeeStatus(id: number, isActive: boolean): Promise<void> {
    return this.httpService.put(`${this.employeesUrl}/status/${id}`, { isActive });
  }

  async getEmployeeById(id: number): Promise<EmployeeModel> {
    const response = await this.httpService.get(`${this.employeesUrl}/${id}`);
    return response.data as EmployeeModel;
  }
}

export interface GetEmployeesResponse {
  employees: EmployeeModel[];
}
